/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const fs = require('fs');
const sjs = require('shelljs');
const path = require('path');
const yaml = require('js-yaml');
class CoffeeConf {
	constructor(db) {
		this.db = db;
		this.dotname = `.${this.db}`;
		sjs.mkdir('-p', this.dotname);
	}

	static access(db) {
		if (sjs.test('-d', `.${db}`)) {
			return new Confjs(`.${db}`);
		} else {
			throw new Error("Directory doesn't exist!");
		}
	}

	mkcfg(cfgname, content, opts) {
		if (content == null) { content = ''; }
		if (opts == null) { opts = {}; }
		if (!opts.as_json) { opts.as_json = false; }
		if (!opts.as_yaml) { opts.as_yaml = false; }
		if (!opts.prepend_newline) { opts.prepend_newline = false; }
		if (!opts.append_newline) { opts.append_newline = false; }
		if (opts.as_json === true) { content = JSON.stringify(content); }
		if (opts.as_yaml === true) { content = yaml.safeDump(content); }
		if (opts.prepend_newline === true) { content = `\n${content}`; }
		if (opts.append_newline === true) { content = `${content}\n`; }
		return fs.writeFileSync(path.join(this.dotname, cfgname), content, {encoding: 'utf8'});
	}

	writecfg(cfgname, content, opts) {
		if (content == null) { content = ''; }
		if (opts == null) { opts = {}; }
		if (!opts.as_json) { opts.as_json = false; }
		if (!opts.as_yaml) { opts.as_yaml = false; }
		if (!opts.prepend_newline) { opts.prepend_newline = true; }
		if (!opts.append_newline) { opts.append_newline = false; }
		if (opts.as_json === true) { content = JSON.stringify(content); }
		if (opts.as_yaml === true) { content = yaml.safeDump(content); }
		if (opts.prepend_newline === true) { content = `\n${content}`; }
		if (opts.append_newline === true) { content = `${content}\n`; }
		return fs.appendFileSync(path.join(this.dotname, cfgname), content, {encoding: 'utf8'});
	}
		

	rmdb() { return sjs.rm('-rf', this.dotname); }

	rmcfg(cfgname) { return sjs.rm(path.join(this.dotname, cfgname)); }

	rmnested(dirname) { return sjs.rm('-r', path.join(this.dotname, dirname)); }

	readcfg(cfgname, opts) {
		if (opts == null) { opts = {}; }
		if (!opts.as_json) { opts.as_json = false; }
		if (!opts.as_yaml) { opts.as_yaml = false; }
		if (opts.as_json === true) {
			return JSON.parse(fs.readFileSync(path.join(this.dotname, cfgname), {encoding: 'utf8'})); 
		} else if (opts.as_yaml === true) {
			return yaml.safeLoad(fs.readFileSync(path.join(this.dotname, cfgname), {encoding: 'utf8'})); 
		} else {
			return fs.readFileSync(path.join(this.dotname, cfgname), {encoding: 'utf8'}); 
		}
	}

	mknested(dirname) { return sjs.mkdir('-p', path.join(this.dotname, dirname)); }

	exist(name) { return fs.existsSync(path.join(this.dotname, name)); }
}


module.exports =
	{CoffeeConf};
