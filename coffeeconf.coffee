fs = require 'fs'
sjs = require 'shelljs'
path = require 'path'
yaml = require 'js-yaml'
class CoffeeConf
	constructor: (db) ->
		@db = db
		@dotname = ".#{@db}"
		sjs.mkdir('-p', @dotname)

	@access: (db) ->
		if sjs.test('-d', ".#{db}")
			return new Confjs(".#{db}")
		else
			throw new Error "Directory doesn't exist!"

	mkcfg: (cfgname, content = '', opts = {}) ->
		opts.as_json = false if not opts.as_json
		opts.as_yaml = false if not opts.as_yaml
		opts.prepend_newline = false if not opts.prepend_newline
		opts.append_newline = false if not opts.append_newline
		content = JSON.stringify(content) if opts.as_json is true
		content = yaml.safeDump(content) if opts.as_yaml is true
		content = "\n#{content}" if opts.prepend_newline is true
		content = "#{content}\n" if opts.append_newline is true
		fs.writeFileSync(path.join(@dotname, cfgname), content, encoding: 'utf8')

	writecfg: (cfgname, content = '', opts = {}) ->
		opts.as_json = false if not opts.as_json
		opts.as_yaml = false if not opts.as_yaml
		opts.prepend_newline = true if not opts.prepend_newline
		opts.append_newline = false if not opts.append_newline
		content = JSON.stringify(content) if opts.as_json is true
		content = yaml.safeDump(content) if opts.as_yaml is true
		content = "\n#{content}" if opts.prepend_newline is true
		content = "#{content}\n" if opts.append_newline is true
		fs.appendFileSync(path.join(@dotname, cfgname), content, encoding: 'utf8')
		

	rmdb: -> sjs.rm('-rf', @dotname)

	rmcfg: (cfgname) -> sjs.rm(path.join(@dotname, cfgname))

	rmnested: (dirname) -> sjs.rm('-r', path.join(@dotname, dirname))

	readcfg: (cfgname, opts = {}) ->
		opts.as_json = false if not opts.as_json
		opts.as_yaml = false if not opts.as_yaml
		if opts.as_json is true
			return JSON.parse(fs.readFileSync(path.join(@dotname, cfgname), encoding: 'utf8')) 
		else if opts.as_yaml is true
			return yaml.safeLoad(fs.readFileSync(path.join(@dotname, cfgname), encoding: 'utf8')) 
		else
			return fs.readFileSync(path.join(@dotname, cfgname), encoding: 'utf8') 

	mknested: (dirname) -> sjs.mkdir('-p', path.join(@dotname, dirname))

	exist: (name) -> return fs.existsSync(path.join(@dotname, name))


module.exports =
	CoffeeConf: CoffeeConf
